puts "PURPOSE:    To Learn  Ruby Programming "
puts "OBJECTIVE:  To complete 14 modules in ruby, as below. "

puts "INTRODUCTIONS: Hello! "

puts "EXPERIENCE:  What relevant experience do you have?"
puts "APPLICATIONS: How will you use ruby? "

puts
puts "SCHEDULE"
puts
puts "QA RUBY PROGRAMMING -- FOUR DAY"

puts %w{
  08.00-17.00
  8.00-09.30
  BREAK
  09.45-11.15
  BREAK
  11.30-12.45
  BREAK
  13.30-15.00
}

# 9.30am - 4pm:       9.30-11.00  B  11.15-12.30am  B  13.15-14.30  B  14.45-16.00

puts
puts
puts " D T  Ch   Title            "
puts
puts " 1 AM 1    Introduction "
puts " 1 AM 2    Fundamentals "
puts
puts " 1 PM 3    Flow"
puts " 1 PM 4    Strings "
puts
puts " 2 AM 5    Collections "
puts " 2 AM 6    Iteration "
puts
puts " 2 PM 7    Regex "
puts " 2 PM 8    Persistence "
puts
puts " 3 AM 9    Def "
puts " 3 AM 10   Class "
puts
puts " 3 PM 11   Include "
puts " 3 PM 12   Errors "
puts
puts " 4 AM 13    Multitasking "
puts " 4 AM 14    Libraries "
puts
puts " 4 PM 15    REVIEW "
puts

puts
puts "PROCESS:                        ~1h 30m / module"

puts "1. demonstration                1 h "
puts "2. review notes: pptx, .rb      5 m "
puts "3. independent exercise        20 m "
puts "4. review exercise              5 m "
